# About

My name is Kevin Matory and this is my Fab Academy documentation website. This page will tell you more who I am and what are my intentions.


## Who am I

I am a member of Cooperation Jackson's CPC (Community Production Center) team.


## My Intentions

I'm here to acquire foundational skills that will help Cooperation Jackson meet its goal of creating a vehicle for community production and promoting self reliance.