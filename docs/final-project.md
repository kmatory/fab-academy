#Final Project: Mobile Stall

In 2019, Bagchee Architects designed a mobile stall for members of Cooperation Jackson's "Freedom Farms". They would use this mobile stall to transport and sell their produce at places such as farmer's markets. It occurred to me that this would make a good, practical final project that would incorporate the use of all the tools.


![Artist conception of project](../images/final_project/mobile_stall_presentation.jpg)

I acquired the CAD file for the mobiles stall from Bagchee Architects, who were kind enough to meet with me and provide some insight on its design, particular in the area of structural integrity under load. This mobile stall has a steel frame, but would come in contact with very rough streets and potholes. We talked about its potential uses and the type of road conditions it will come in contact with; whether any additional bracing would be needed for the frame. The author assured those concerns were incorporated into the design and felt strongly that nothing more was needed. However, she did stress that I find a good welder.


##Use scenarios
The uses are pretty straight forward. The Freedom Farms team intend to take this stall to remote areas where they would set up shop to sell their produce. It would allow them to interact directly with people who are interested in fresh produce locally grown without the use of pesticides or other harmful chemicals. 


##Additional Design Considerations

It also makes sense to add some temperature controls, humidity sensors and data logging which could provide some useful information in determining possible remaining shelf life of the produce. This stall, as it's moving, could encounter a lot of buffeting and jarring forces from poor street conditions which could contribute to damaged goods, so an accelerometer could provide some insight to how one might adjust speed and handling of the road.



##Parts List
<iframe src="../embed/parts_list/materials_table.html" style="width:100%;height:475px;"></iframe>



##Rendering and Animation 

![Rendering of CAD model](../images/final_project/mobile_stall_render.jpg)

<figure class="video_container">
	<video controls="controls" src="../videos/final_project/mobile_stall_animation_website.mp4" type="video/mp4" width="100%">
        Animation of CAD model
    </video>
</figure>


This step is instrumental in highlighting potential problems and overlooked details in the design or CAD model. I've listed my observaions below.


###Roof structures
This design calls for the use of 3 hydraulic pistons on each side to support the weight of the roof while extended but the pistons were not added in the CAD model. Additionally, I believe the roof could use a latch to prevent the stable during travel. The roof will be offset from the top of the stall by the cross bracing it will be attached to, therefore, its probably a good idea to add wind deflectors to this area to prevent the roof catching air during travel.

###Shelves
The shelves are intended to fold out for use and fold back in for travel, however in this model, the shelves are missing attachments mechanisms, support structures and latches. They are free floating.

###Back Doors
The back doors are also free floating, do not have hinges or latches. The width of the doors may need to be adjusted to offset for the hinges that will have to be added.

###Food containers
The food containers will be milled from HDPE. The only detail I'd add is milling out handles on the sides to make them easier to pick up and carry.  


##Remodeling the trailer
The CAD file I recevied is just a collection of bodies and therefore can't be easily scaled. Idealy, I'd like to  have a parametric design, so I'm remodling this design from scratch.

###Problems while remodeling

I ran into a number of problems while remodeling the stall.

  1. I noticed within the CAD model, the width of the steel angles are 1 to 2 inches. In the parts list, the width of steel angles called for is 3 inches or 3 X 3 X 3/16".

  2. The length of trailer in the CAD model is much longer than what was stated in the specs sheet I was given. There was also a slight difference in the trailer widths. I will have to personally verify the dimensions of the trailer that is to be used.

  3. The stall will have 2 swing doors and a back panel made of steel. The model has a different thickness than what the parts list calls for.

  4. This CAD model will have to be recreated from scratch to be parametric. In addition, I will have to enlist the help of a welder who understands and knows steel angles to model this mobile stall correctly.


##Update on the CAD model
It was confirmed to me that the CAD model was just a proof of concept to demonstrate how the parts fit together. This model was not created to scale. This confirms that I will definitely have to remodel from scratch.


##Lining up welding talent
Previously, an associate confirmed with me that he'd be able to help with this project. I was relying on him to connect me with someone in his network that could handle this welding for me. Unfortunately, I've not been able to reach him or get a call back since our first conversation regarding this project so I will have to assume that he is no longer on board. This will push back my timeline.

##Design and Talent problems
I talked to a guy who runs a machine shop, hoping they could fabricate the frame for this mobile stall. I brought the design in for him to look at and he pointed out 2 problems.

  1. He was concerned about the weight of this design; that it appears to be really heavy.
  2. The cost for a machine shop such as his to fabricate this would be in the neighborhood of $30k.

With that said, I'll do some calculations and get an overview of the total weight of the mobile stall including load, look into a different sector of welder-fabricators for my talent and avoid the overhead of a machine shop. 

##Exploring weight and design considerations
Calculating some rough weight estimates of the mobile stall under load brought to light 3 problems.

  1. The Gross Vehicle Weight Rating is 2990 lbs. The weight of the steel frame plus the trailer itself and extras comes in at 2156 lbs. This means there is only 834 lbs remaining for the load. The original design files of this mobile stall suggests a particular purpose and use case scenario. This mobile stall will hold a total 36 produce containers of various sizes. Based on some rough calculations, the total weight of all the produce comes to 1587 lbs. This weight does not include the weight of the containers themselves. This would put the trailer at 753 lbs over weight limits. 

  2. The end user stated that they would only carry anywhere from 50 to 200 lbs of produce. This would be well within the 834 lbs load that this design could carry and would solve the problem stated in the first point, but this highlights a discripency between the original design which had a much higher expected load of 1587 lbs. If the expected maximum load at any time is 200 lbs, then why the big gap between design and use. This big of a difference needs to be reconciled. At a minimum, with this design, a user would be pulling another 2156 lbs of weight in addition to the hauling truck's own weight. This is a lot of weight to pull, significant resources and gas investment for only 200 lbs of produce. This difference would eat up any potential sales profits.

  3. The third problem is connected to the first. Looking at the numbers, 72% of the G.V.W.R will consumed by the weight of the steal frame. The remaining 28% of the weight will for the load. This load ratio isn't very efficient so the question is, where and how can we shed weight, make this design lighter to sufficiently improve that ratio or, do we need to reconsider the design altogether based on the use case scenario.

  Based on these points, we had a meeting with the end user to discuss use cases and further explore the details of those scenarios. With an adequate understanding of how this is to be used, we can then better determine what modifications, if any, the current design needs to be a better fit for those uses.

##Container Weights

<iframe src="../embed/weight_calculations/produce_containers.html" style="width:100%;height:270px;overflow: hidden;"></iframe>

##Material Weights

<iframe src="../embed/weight_calculations/material_weights.html" style="width:100%;height:350px;"></iframe>

##Overall Structure Weight

<iframe src="../embed/weight_calculations/trailer_structure.html" style="width:100%;height:220px;"></iframe>


##Making A Scale Model

####Materials:
<p>- 3/8 and 3/16 balsa wood</p>




