//
// hello.ftdi.45.echo.morse.c
//
// tiny45 echo hello-world
//    9600 baud FTDI interface
//		modifed w/ button & led for morse code for "Redraw one of the echo hello-world boards"
//
// Neil Gershenfeld 11/17/19
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//
//  make -f hello.ftdi.45.echo.morse.make program-ice avr-objcopy -o ihex hello.ftdi.45.echo.morse.out hello.ftdi.45.echo.morse.c.hex
//  python -m serial.tools.miniterm

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#define output(directions,pin) (directions |= pin) // set port direction for output
#define input(directions,pin) (directions &= (~pin)) // set port direction for input
#define set(port,pin) (port |= pin) // set port pin
#define clear(port,pin) (port &= (~pin)) // clear port pin
#define pin_test(pins,pin) (pins & pin) // test for port pin
#define bit_test(byte,bit) (byte & (1 << bit)) // test for bit set
#define bit_delay_time 102 // bit delay for 9600 with overhead
#define bit_delay() _delay_us(bit_delay_time) // RS232 bit delay
#define half_bit_delay() _delay_us(bit_delay_time/2) // RS232 half bit delay

#define serial_port PORTB
#define serial_direction DDRB
#define serial_pins PINB
#define serial_pin_in (1 << PB2)
#define serial_pin_out (1 << PB1)
#define led_pin_out (1 << PB4) //pin to the LED
#define button_pin_in (1 << PB3) //pin to the button
#define max_chr_input_buffer 25  //limit number of input characters
#define max_chr_output_buffer (max_chr_input_buffer * 5) //limit number of morse codes characters
#define strobe_delay() _delay_ms(50)// how long to hold led low between letters
#define dot_delay() _delay_ms(100)// how long to hold LED high to represet dot
#define dash_delay() _delay_ms(200)// how long to hold LED high to represet dash
#define space_delay() _delay_ms(300)// how long to hold the led low to represent space

void runLoop();

char code_output_buffer[max_chr_output_buffer];
static const char codeTable[26][5] PROGMEM = {
".-",//a > 97
"-...",//b > 98
"-.-.",//c > 99
"-..",//d > 100
".",//e > 101
"..-.",//f > 102
"--.",//g > 103
"....",//h > 104
"..",//i > 105
".---",//j > 106
"-.-",//k > 107
".-..",//l > 108
"--",//m > 109
"-.",//n > 110
"---",//o > 111
".--.",//p > 112
"--.-",//q > 113
".-.",//r > 114
"...",//s > 115
"-",//t > 116
"..-",//u > 117
"...-",//v > 118
".--",//w > 119
"-..-",//x > 120
"-.--",//y > 121
"--..",//z > 122
};

const char pressBtn[ ] PROGMEM = "~PRESS BUTTON~\n\r";
const char msgEnter[ ] PROGMEM = "Type message and press enter: ";
int chrCnt;
int spPosAr[max_chr_input_buffer];


void get_char(volatile unsigned char *pins, unsigned char pin, char *rxbyte) {
   //
   // read character into rxbyte on pins pin
   //    assumes line driver (inverts bits)
   //
   *rxbyte = 0;
   while (pin_test(*pins,pin));
	//
	// wait for start bit
	//

   //
   // delay to middle of first data bit
   //
   half_bit_delay();
   bit_delay();
   //
   // unrolled loop to read data bits
   //
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 0);
   else
      *rxbyte |= (0 << 0);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 1);
   else
      *rxbyte |= (0 << 1);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 2);
   else
      *rxbyte |= (0 << 2);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 3);
   else
      *rxbyte |= (0 << 3);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 4);
   else
      *rxbyte |= (0 << 4);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 5);
   else
      *rxbyte |= (0 << 5);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 6);
   else
      *rxbyte |= (0 << 6);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 7);
   else
      *rxbyte |= (0 << 7);
   //
   // wait for stop bit
   //
   bit_delay();
   half_bit_delay();
}

void put_char(volatile unsigned char *port, unsigned char pin, char txchar) {
   //
   // send character in txchar on port pin
   //    assumes line driver (inverts bits)
   //
   // start bit
   //
   clear(*port,pin);
   bit_delay();
   //
   // unrolled loop to write data bits
   //
   if bit_test(txchar,0)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,1)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,2)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,3)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,4)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,5)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,6)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,7)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   //
   // stop bit
   //
   set(*port,pin);
   bit_delay();
   //
   // char delay
   //
   bit_delay();
}

void put_string(volatile unsigned char *port, unsigned char pin, char *str) {
   //
   // print a null-terminated string
   //
   static int index;
   index = 0;
   do {
      put_char(port, pin, str[index]);
      ++index;
      } while (str[index] != 0);
}

void put_string_P(volatile unsigned char *port, unsigned char pin, const char *data)
{
	while (pgm_read_byte(data) != 0x00)
	  put_char(port, pin, pgm_read_byte(data++));
}

void setLED() {
	set(serial_port, led_pin_out);
}

void clearLED() {
	clear(serial_port, led_pin_out);
}


void strobeLED() {
	int j=0;
	
	if( code_output_buffer[0] != '\0' ) 
	{
		put_char(&serial_port,serial_pin_out,10); // new line
		put_char(&serial_port,serial_pin_out,10); // new line

		for(int i=0;i<chrCnt;i++) 
		{
			if(spPosAr[j] == i) {//put a space between morse code letters
				put_char(&serial_port, serial_pin_out, 32);
				j++;
			}
			
			unsigned int x = (code_output_buffer[i] + 0);		
			strobe_delay();
			
			switch(x) 
			{
				case 46:
					put_char(&serial_port, serial_pin_out, 46);
					setLED();//flash dot
					dot_delay();
					clearLED();					
				break;
				
				case 45:
					put_char(&serial_port, serial_pin_out, 45);				
					setLED();//flash dash
					dash_delay();
					clearLED();
				break;
				
				case 32:
					put_char(&serial_port, serial_pin_out, 32);				
					clearLED();//space between words
					space_delay();
				break;
				
				default:
					
				break;
			}					
		}
	}
	
	put_char(&serial_port,serial_pin_out,10); // new line
	runLoop();	
}


void runLoop() {
	//
	// main loop
	//
	char chr;   
	int index = 0, k=0;
	chrCnt = 0;

	char *cp;
	cp =& code_output_buffer[0];
	put_string_P(&serial_port, serial_pin_out, msgEnter);

	while(1) 
	{		
		get_char(&serial_pins,serial_pin_in,&chr);

		if(chr == '\n' || chr == '\r')
		{
			put_char(&serial_port,serial_pin_out,10); // new line
			put_string_P(&serial_port, serial_pin_out, pressBtn);
			//
			// wait for button down
			//
			while (0 != pin_test(serial_pins, button_pin_in));

			//
			// wait for button up
			//
			while (0 == pin_test(serial_pins, button_pin_in));
			
			strobeLED(); 
		}
		
		put_char(&serial_port,serial_pin_out, chr);		
		int codeNo = (chr - 97);

		for(int j=0;j<5;j++) 
		{			
			if(chr == 32) {
				*cp = 32;
			} else {
				*cp = pgm_read_word(&codeTable[codeNo][j]);
			}
			cp++;
			chrCnt++;			
		}
		
		spPosAr[k++] = chrCnt;//tracking spaces between letters
		
		if (index == (max_chr_input_buffer-1)){ index = 0; }
	}	
}

int main(void) {
	//
	// set clock divider to /1
	//
	CLKPR = (1 << CLKPCE);
	CLKPR = (0 << CLKPS3) | (0 << CLKPS2) | (0 << CLKPS1) | (0 << CLKPS0);

	//
	// initialize pins
	//
	set(serial_port,serial_pin_out);
	output(serial_direction,serial_pin_out);

	//set the button pin low and as an input
	set(serial_port, button_pin_in); // turn on pull-up
	input(serial_direction, button_pin_in);

	//set the led low (initially) and as an output
	clear(serial_port, led_pin_out);
	output(serial_direction, led_pin_out);
	
	runLoop();

	return 0;
}
