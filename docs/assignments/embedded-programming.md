Embedded Programming

    Group assignment:
        Compare the performance and development workflows for other architectures
        Document your work to the group work page and reflect on your individual page what you learned

    Individual assignment:
        Read the datasheet for your microcontroller
        Use your programmer to program your board to do something

Learning outcomes

    Identify relevant information in a microcontroller datasheet.
    Implement programming protocols.

<!--
Have you answered these questions?

    Link to the group assignment page
    Documented what you learned from reading a microcontroller datasheet.
    Programmed your board
    Described the programming process/es you used
    Included your source code
    Included a short ‘hero video’ of your board

FAQ
Is it enough for the group assignment if we compare different microcontrollers in theory?

    Answer: No, You need to program at least two different family MCU's.

Do I have to design and make a new board for this week

    Answer: No, You can use the board you made in electronics design

If I adjust existing code, is that now my code?

    Answer: If you use someone's else code as a starting point, remember that you must acknowledge whoever made it. See General Essentials. To succeed in this assignment, experiment with changing the code, understand how that worked, and write your own.

How do I prove I've read the Datasheet?

    Answer: Point out things in your code and board design that you learnt from the Datasheet. Also point to other weeks when you used information from the Datasheet.

How should I put my code on my website?

    Answer: As a file for download, same as all your other assignments. Keep your webpage tidy and easy to read.
-->


Add some discussion about the data sheet about relevant information
AVRDUDE, MAKEFILE for programming



The second part of the initial test involves setting the PB4 pin on the T45 as an output and additionally set it to HIGH to put 5V on the LED to verify the LED is indeed functional.

Though I didn't do a preliminary test on the button itself, I was satisfied the board was ready and therefore any problems I might run into with coding wouldn't be reflective of the board itself but a problem with my code.

<figure class="video_container">
    <video controls="controls" src="../../videos/embedded_programming/hello.ftdi.45.echo.morse-demo.mp4" type="video/mp4" width="100%">
        Board Demo
    </video>
</figure>


##Downloads:

[Click here for the C code](../../files/electronics_design/hello.ftdi.45.echo.morse.c){target=_blank}