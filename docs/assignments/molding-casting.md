# Molding and Casting

[A link to the assignment page](http://academy.cba.mit.edu/classes/molding_casting/index.html){target=_blank}

Group assignment:

 - [Review the safety data sheets](#review-the-safety-data-sheets) for each of your molding and casting materials

 - [Make and compare test casts with each of them](#test-cast)

Individual assignment:
   
 - [Design a 3D mould](#design-a-3d-mould) aroundthe stock and tooling that you'll be using, mill it (rough cut + (at least) three-axis finish cut), and [use it to cast parts](#use-it-to-cast-parts).
    - Learning outcomes
        - Design appropriate objects within the limitations of 3 axis machining
        - Demonstrate workflows used in mould design, construction and casting

<!--
Have you answered these questions?

    Linked to the group assignment page and reflected on your individual page what you have learned
    Reviewed the safety data sheets for each of your molding and casting materials, then made and compared test casts with each of them
    Documented how you designed your 3D mould and created your rough and finish toolpaths for machining, including machine settings
    Shown how you made your mould and cast the parts
    Described problems and how you fixed them
    Included your design files and ‘hero shot’ of the mould and the final object
-->

## Review the safety data sheets

I'll make my soft mold from [OOMO 25](https://www.smooth-on.com/documents/). I'll cast in that mold with [USG Drystone](https://www.usg.com/content/usgcom/en/products/industrial/art-statuary/drystone-casting-media.html) and I've reviewed the SDS for both products.



<!--

Design appropriate objects within the limitations of 3 axis machining
Demonstrate workflows used in mould design, construction and casting

Design a 3D mould around the stock and tooling that you'll be using, mill it (rough cut + (at least) three-axis finish cut), and use it to cast parts.

-->

## Design a 3D Mould

These are the tools I'll use to mill the mold.

- 1/8"  - 4 flute Carbi-Universal flat square for roughing
- 1/32" - 2 flute Carbi-Universal flat square for finishing

I'll be milling the mold from a block of wax that is 7"L x 3"W x 1.5"H, so I have to be sure that my design will fit inside those dimensions with room to spare on the sides and top to accomodate an adequate amount of OOMO 25.

This is a heavily modified version of the Cooperation Jackson logo. It's simplified to ensure that the 1/32" tool can get in between the details of the piece. That meant making sure that the spacing between details is no less than 1 &#247; 32 or 0.03125".

![logo-sketch.jpg](../../images/molding_casting/logo-sketch.jpg)

![sketch-dimension-1.jpg](../../images/molding_casting/sketch-dimension-1.jpg)

![sketch-dimension-2.jpg](../../images/molding_casting/sketch-dimension-2.jpg)

This is the final mold which is 0.95" high. The walls are 0.138" thickness near the top edges. There is a minimal of 0.278" of spacing between the piece and the walls so the 1/8" tool can easily fit. As per the OOMO 25 instructions, there is a little more than 1/2" of space from the highest part of the piece to the top of the mold.
This will be exported in an stl format.

![sketch-dimension-2.jpg](../../images/molding_casting/final-mold.png)

Modela Player 4

I've imported the model stl into Modela Player 4 to create the toolpathing and set up the parameters for the rough and finish toolpathing.

![stl-import.png](../../images/molding_casting/stl-import.png)

Perform a simulation to check for any potentional problems.

![check-simulation.png](../../images/molding_casting/check-simulation.png)

I'll zero the X,Y on the center of my piece, but to do that, I first I need to zero the user X,Y origin to that of the machine X,Y coordinates so that user origin will be relative to that of the machine origin.

![machine-user-origin-sync.png](../../images/molding_casting/machine-user-origin-sync.png)

The wax is secured and centered.

![securing-wax-block.jpg](../../images/molding_casting/securing-wax-block.jpg)

I marked the center point of where I would start milling.

![marking-center-origin.jpg](../../images/molding_casting/marking-center-origin.jpg)

I've finished the roughingvand removed much of the excess wax shavings. It's now time to start the finishing pass.

![milling-wax-complete.jpg](../../images/molding_casting/milling-wax-complete.jpg)

I've completed milling the positive and cleaned out the mold. Around the edges of the piece and can see indications of melted wax. I could probably eliminate that by reducing the speeds. Some speed tests would help me find the right speeds.

![milling-result.jpg](../../images/molding_casting/milling-result.jpg)

Adding mold release to make extraction easier

![mold-release-agent.jpg](../../images/molding_casting/mold-release-agent.jpg)

Mixing the OOMOO 25 part A and B.

![mixing-oomoo.jpg](../../images/molding_casting/mixing-oomoo.jpg)

Pouring the OOMOO into the mold.

![pouring-oomoo.jpg](../../images/molding_casting/pouring-oomoo.jpg)

The OOMOO has cured.

![mold-set.jpg](../../images/molding_casting/mold-set.jpg)

I started extraction by pealing it away from the edge.

![extracting-mold.jpg](../../images/molding_casting/extracting-mold.jpg)

Here's my the negative mold. I'll cast the drystone into this mold.

![negative-mold.jpg](../../images/molding_casting/negative-mold.jpg)

## Use it to cast parts

Mixing the drystone. The recommended ratio is 100 lb. of Drystone to 18-20 lb. of water which is roughly a 5 to 1 ratio. I used a 4 to 1 ratio to reduce viscocity because of how small the mold and the details are.

![drystone-mixing.jpg](../../images/molding_casting/drystone-mixing.jpg)

### Test cast

I did a test cast and the results are not great. There is evidence of air bubbles and the drystone didn't get into the details and corners well.

![drystone-test-cast-wet.jpg](../../images/molding_casting/drystone-test-cast-wet2.jpg){width=325 style=float:left;}

![drystone-test-cast-dry.jpg](../../images/molding_casting/drystone-test-cast-dry.jpg){width=325}

### Final cast

I did another cast, taking out time to push the Drystone into the corners and details a little before finishing. The result isn't perfect, but much better this time.

![drystone-final-cast-wet.jpg](../../images/molding_casting/drystone-final-cast-wet.jpg){width=360 style=float:left;}

![drystone-final-cast-dry.jpg](../../images/molding_casting/drystone-final-cast-dry.jpg){width=320}

<!--
Limit the tool selection to those that I will be using
Go through the setup process of setting the rough and finish cuts
Select the tools in that process
Select the area of milling in that process
Simulate the milling process with Modela Player to check for problems
View the simulated results, make adjustments if necessary, repeat until ready to mill
Zero out the X,Y origin to be the same as the machine origin
Insert the centered piece
Mark the center of the piece where I will set my new X,Y origin relative to the machine 0,0 origin
Set the X,Y on the center marking and set Z origin
Run cuts and follow instructions for tool change when required and reset the Z origin at tool change
Continue cuts
-->
