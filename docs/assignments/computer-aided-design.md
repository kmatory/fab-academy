#Computer Aided Design

[Assignment page](http://academy.cba.mit.edu/classes/computer_design/index.html){target=_blank}

assignment

 - model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project,
 - compress your images and videos,
 - and post a description with your design files on your class page

Computer-Aided Design

    Model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project, compress your images and videos, and post it on your class page

Learning outcomes

    Evaluate and select 2D and 3D software
    Demonstrate and describe processes used in modelling with 2D and 3D software

Have you answered these questions?

    Modelled experimental objects/part of a possible project in 2D and 3D software
    Shown how you did it with words/images/screenshots
    Included your original design files

FAQ
Is it compulsory to do parametric design this week?

    Answer: No, but it is in computer controlled cutting. So it is a good idea to start learning now.

<hr />




