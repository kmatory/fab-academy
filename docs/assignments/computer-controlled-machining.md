# Computer-Controlled Machining

<style>
h2{text-decoration: underline;}
</style>

[Assignment page](http://academy.cba.mit.edu/classes/computer_machining/index.html){target=_blank}

### Group assignment:

- [Complete your lab's safety training](#complete-your-labs-safety-training)
- [Test ... for your machine](#test-for-your-machine)
    - [Runout](#runout)
    - [Alignment](#alignment)
    - [Speeds](#speeds)
    - [Feeds](#feeds)
    - [Toolpaths](#toolpaths)
- Document your work to the group work page and reflect on your individual page what you learned

### Individual project

- [Make &#40;design&nbsp;+&nbsp;mill&nbsp;+&nbsp;assemble&#41; something big](#make-something-big)
    - [testing joints](#testing-joints)
    - [setting up the machine](#setting-up-the-machine)
    - [using fixings](#using-fixings)
    - [adjusting feeds, speeds, depth of cut](#adjusting-feeds-speeds-depth-of-cut)
    - Learning outcomes
        - Demonstrate 2D design development for CNC production
        - Describe workflows for CNC production
<!--
Have you answered these questions?

Linked to the group assignment page
Documented how you designed your object (something big)
Documented how you made your CAM-toolpath
Documented how you made something BIG (setting up the machine, using fixings, testing joints, adjusting feeds and speeds, depth of cut etc.)
Described problems and how you fixed them
Included your design files and ‘hero shot’ photos of final object

FAQ
How big is big?

Answer: Life size, big enough to show you understand many of the possibilities of CNC machining - drill, pocket, dogbones, nesting, etc.

Does it have to be wood or wood products?

Answer: No. But the lab is only responsible for providing you with a full size wood board (4x8 ft).
-->
<hr />

CNC Machine:

- ShopBot

Toolpathing software:

- Vectric VCarve

<hr />

## Complete your lab's safety training
- I've completed my lab saftey training. 

## Test ... for your machine

### Runout

I took the opportunity to take measurements to check the runout while testing for [alignment](#alignment). 

Kerf measurement: 6.3 mm
![Kerf Measurement](../../images/computer_controlled_machining/runout-kerf-measurement.jpg)

Tool measurement: 6.2 mm
![Tool Measurement](../../images/computer_controlled_machining/runout-endmill-measurement.jpg)

<p style="padding:10px;border:2px solid #000000; background-color:#fcfcec;">
&nbsp;&nbsp;6.3 &nbsp;&nbsp;&nbsp;(kerf) <br />
<u>- 6.2&nbsp;&nbsp;&nbsp;</u> (tool diameter)<br />
= 0.1 mm &nbsp;/&nbsp;0.003937 in of runout
</p>
 
### Alignment

![square-measurement-1.jpg](../../images/computer_controlled_machining/square-measurement-1.jpg){width=300}
![square-measurement-2.jpg](../../images/computer_controlled_machining/square-measurement-2.jpg){width=300}

<p style="padding:10px;border:2px solid #000000; background-color:#fcfcec;">The measurement is 4.25 inch, corner to corner, in both directions.</p>

### Speeds

Tool: 1/4" down cut

Rpm: 14000


### Feeds

Tool: 1/4" down cut

Rpm: 14000


### Toolpaths

* Software:
    - Vectric VCarve

Toolpath generation is the process of taking a design and translating it into a format that instructs the CNC machine how to make the necessary cuts to produce the desired end result.

<hr />

## Make Something Big

##### Coffee Table

 - dimensions: 46" L x 21" W x 21.7" H

##### Materials

 - 3/4" plywood (0.70 in)
 - 0.19" cardboard

##### Software

 - Autodesk Fusion 360
 - VCarve Pro - ShopBot Edition 9.5

##### Downloads

<ul>
<li><a href="../../files/computer_controlled_machining/Coffee-Table.f3d" target="_blank">Fusion 360 design file</a></li>
<li><a href="../../files/computer_controlled_machining/Coffee-Table.crv" target="_blank">VCarve toolpaths file</a></li>
</ul>

##### Rendering

This is rendering of the table made held together with press fit joints.

![coffee table render](../../images/computer_controlled_machining/coffee-table-render.jpg)

##### Testing Joints

Before I start designing, I need to carry out a compression test with the plywood I'm using to determine the tolerance needed for effective joints. The tolerance will tell me how much clearance or overlap needed in the joints for the desired fit. 

Since I'll make a scale model first from cardboard, I'll also need to keep in mind the kerf of the lasermachine already determined in [Computer Controlled Cutting](/fab-academy/assignments/computer-controlled-cutting/#kerf){target=_blank} as 0.007 inch. The model is just to further reveal any problems with my design, so I won't be simulating the joints and therefore, the tolerance for cardboard joints isn't needed.

For this test, I modeled 3 blocks being sure to account for the runout. Each block has a channel that is 0.70" wide, which is the thickness of the plywood. An additional width of 0.01", 0.015", 0.02" was added to to each channel incrementally, resulting in 3 block with channels of varying width. The goal is to check each block to see which joint channel has the right amount of compression.

In the image below, the width of the channels are adjusted for runout. Runout would make the channel slightly wider, so if we look at the last block on the right, for example, the width is 0.716" after the runout has been subtracted to offset.  Normally it would be 0.72" or 0.70" (plywood thickness) + 0.02 (clearance).

![plywood compression test](../../images/computer_controlled_machining/plywood-joint-test-model.jpg)

![plywood compression test](../../images/computer_controlled_machining/plywood-compression-joint-test.jpg)

Oversizing the channel by 0.02" gives the best result. The other test channels proved to be too narrow to fit a piece in without considerable force.

![plywood compression test](../../images/computer_controlled_machining/plywood-cjt-plus02.jpg)


##### Designing

Now that I know the tolerance, kerf (needed for the lasermachine), and additionally the runout, I can add them to a list of parameters I'll use quite often in the design of this table. Below is a screenshot of my complete list.

![plywood compression test](../../images/computer_controlled_machining/parameters.jpg)

I took a bottom up approach and started with the bottom piece. The following image shows the dimensions of the piece and how runout and kerf was accounted for in the dimensioning.

The red square shows the table is 46" long total, but since the laser machine cuts directly on the toolpath, this dimension would be slightly decreased by laser kerf. Additionally, the runout on the Shopbot would further decrease this dimension, so to account for both, I add both the kerf and runout to 46" on the front end to offset.

Below, the purple square shows the math used to account for the kerf and runout. In this instance, the base width of the channel is 0.70". The kerf and runout would increase the width of the channel during the fabrication process, so to offset and get the width back down to 0.70" (represented by parameter "matThickness" - material thickness), I subtract both kerf and runout from 0.70" on the front end. Lastly, I need to adjust this 0.70" channel for "tolerance". As already stated, adding another 0.02" to the 0.70" width of the channel gives me the clearance I need between the pieces for a good fit. In total, when fabricated, the channel width is 0.72".

![f360-parameters-example.jpg](../../images/computer_controlled_machining/f360-parameters-example.jpg)

This is the approach I took throughout the designing process, stopping to think about how my design will be affected by the characterization of the machines used and compensating for those factors. This table has 4 main pieces, 3 of which were mirrored to form the remain parts of the table.

![f360-table-view.png](../../images/computer_controlled_machining/f360-table-view.jpg)


###### Scale model

A scale model can show potential problems with the design that was missed during the design process.
There are some parameters I need to change for the model: 

 - kerf: 0.007"
 - runout: 0 (n/a)
 - tolerance: 0 (n/a - would be -0.018 but glue will be used in place of joints)
 - matThickness (material thickness): 0.19" x 6 = 1.14"

Even though I won't be utilizing joints in this scale model, I will have them etch in and this will help me easily align the pieces without having to eyeball it or measure it out.

To be thorough, I want to also address another problem with a scale model in regards to material thickness. The thickness of the cardboard is 0.19". I intend to do a 1:6th scale model. This means that if I do not modify the material thickness, it will scale down by a factor of 1:6, that is 0.19/6 = 0.031, making the channels only a fraction of the thickness of the cardboard. In order to properly scale down the material thickness, I need to first multiply it by a factor of 6, that is, 0.19" x 6 = 1.14" and assign this to the parameter "matThickness". Now when I export a 1:6 scale drawing, it will be 0.19".

Though the image below shows several pieces in 2.5D, I exported each piece individually in the 2D format as a .dxf file.

![f360-table-drawing-exports.jpg](../../images/computer_controlled_machining/f360-table-drawing-exports.jpg)

###### Printing to the lasermachine

Each .dxf file was imported into Corel Draw (evaluation version) and layed out. I converted the lines that need to be cut to a "hairline" thickness and the joint channels were left as-is so they will be etched instead of cut. The etched lines will help me with placement and alignment when I go to glue the pieces together.

![corel-model-pieces-layout.png](../../images/computer_controlled_machining/corel-model-pieces-layout.png)

This is the dialog box that pops up when printing to the lasermachine. We have a CO<sub>2</sub> laser, I have both vector and raster lines, the bed is 32"Lx20"W, the raster settings were left at default, and the vector settings are 23s, 95p, 60f. I uncheck speed comp and/or power comp if they are checked as those settings aren't relevant here.

![corel-model-print-settings.png](../../images/computer_controlled_machining/corel-model-print-settings.png)


![manual-focus-guage-setting.jpg IMAGE PLACEHOLDER](../../images/computer_controlled_machining/manual-focus-guage-setting.jpg)

I didn't do a perfect job of gluing the model together but everything checks out. Below is the resulting cardboard model.

![coffee-table-cardboard-model.jpg](../../images/computer_controlled_machining/coffee-table-cardboard-model.jpg)

### Toolpathing for Shopbot

##### Software

 - VCarve Pro - ShopBot Edition 9.5

As with the model, I exported the pieces again in the .dxf format, this time in a 1:1 scale, and imported them into VCarve. 

##### Using fixings

Below, the red circles indicate the placements for peck drilling my fixtures, which are screws in this case. The blue arrows point to the layout of pieces.

![coffee-table-vcarve-toolpaths-1.jpg](../../images/computer_controlled_machining/vcarve-coffee-table-fixtures.jpg)

Not including the fixtures, I basically have 2 types of toolpaths, pockets and profiles. I have organized these toolpaths into layers (Ref. 2 in image below). It makes sense to do the pockets first (Ref. 3) and profiles afterwards. I needed to make sure that certain pockets extended to the very edge of the piece, so I extended the toolpath slightly beyond the boundary of the piece as in Ref. 1 in the image below. 

![vcarve-screenshots-1.jpg](../../images/computer_controlled_machining/vcarve-screenshots-1.jpg)

The current layout shows the first run which will completely exhaust the 4'x8' sheet of plywood, so the shelves, which were placed on a layer by themselves, will be a second run.

![vcarve-shelves-toolpaths.jpg](../../images/computer_controlled_machining/vcarve-shelves-toolpaths.jpg)


##### Adjusting feeds, speeds, depth of cut

Pockets:

 - tool: 1/4" Down-cut (57-910)
 - speeds: 12000 rpm
 - feeds: 3.0 in/s
 - cut depth: 0.35 in
  - pass depth:  0.125 in (1/2 of tool diameter)
  - passes: 3

Profiles:

 - tool: 1/4" Down-cut (57-910)
 - speeds: 14000 rpm
 - feeds: 3.0 in/s
 - cut depth: 0.75 in
  - pass depth:  0.25 in (tool diameter)
  - passes: 3

<br />

<p>Here is an overview of the order of operations.</p>

<dl>
   <dt style="float: left;width: 300px;">    
      <ol>
         <li>
            Air cut drill pecks
            <ul>
               <li style="background-color: #fae7e7;">Run drill pecks and apply fixtures</li>
            </ul>
         </li>
         <li>
            Air cut pockets
            <ul>
               <li style="background-color: #fae7e7;">Cut Pockets - first side</li>
            </ul>
         </li>
         <li>
            Air cut profiles
            <ul>
               <li style="background-color: #fae7e7;">Cut Profiles</li>
            </ul>
         </li>
         <li>
            Flip and fixture the double sided pieces in place
            <ul>
               <li>Air cut pockets again, to be safe</li>
               <li style="background-color: #fae7e7;">Cut Pockets - second side</li>
            </ul>
         </li>
         <li>
            Clean up table
            <ul>
               <li>Fixture new material to the table<br />(includes drill pecks)</li>
               <li style="background-color: #fae7e7;">Cut Profiles for shelves<br />(includes air cuts)</li>
            </ul>
         </li>
      </ol>
   </dt>
   <dd>
      <img alt="vcarve-order-of-ops.jpg" src="../../images/computer_controlled_machining/vcarve-order-of-ops.jpg" width="280" />    
   </dd>
</dl>

<br style="clear: both" />

##### Setting up the machine

- Check the table for debris
- Check the gantry for movement
- Run X,Y limits
- Run spindle warmup routine
- Install collet and milling tool
- Set X,Y,Z origins
- Run the .sbp toolpaths file in the order specified earlier

Stages 1-3 have been completed. I've cut the pockets on the first side and profiles. The next stage is to flip the doubles sided pieces over for another pocket run. Of course I stopped to carefully vacuum up excess sawdust.

![shopbot-stage-1.jpg](../../images/computer_controlled_machining/shopbot-stage-1.jpg)

I wanted to affix the double sided pieces without using screws, so I decided on using strips of plywood or "kerf fillers" that were 1/4" in thickness. These were made previously made from plywood scraps. They filled the kerf and locked the double sided pieces in place as shown in the image below.

I paid particular attention to keeping the surface level of the piece consistent to that of the surrounding plywood as best I could in an effort to prevent sloping.

![shopbot-kerf-fillers.jpg](../../images/computer_controlled_machining/shopbot-kerf-fillers.jpg)

Once all the pieces were cut, assembly was just a matter of slotting the pieces together.

![table-joint-closeup.jpg](../../images/computer_controlled_machining/table-joint-closeup.jpg)

![coffee-table-assembled.jpg](../../images/computer_controlled_machining/coffee-table-assembled.jpg)

## Downloads

<ul>
<li><a href="../../files/computer_controlled_machining/Coffee-Table.f3d" target="_blank">Fusion 360 design file</a></li>
<li><a href="../../files/computer_controlled_machining/Coffee-Table.crv" target="_blank">VCarve toolpaths file</a></li>
</ul>
