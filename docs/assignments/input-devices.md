#Input Devices

[A link to the assignment page](http://academy.cba.mit.edu/classes/input_devices/index.html){target=_blank}


The second half of the Fab Academy programme is designed to build on the previous weeks. You will be synthesising information and implementing skills that you were introduced to in the first half of the programme and encouraged to integrate these into your final project proposal.

    Group assignment:
        Probe an input device(s)'s analog and digital signals
        Document your work to the group work page and reflect on your individual page what you learned

    Individual assignment:
        Measure something: add a sensor to a microcontroller board that you have designed and read it.

Learning outcomes

    Demonstrate workflows used in sensing something with input device(s) and MCU board

Have you answered these questions?

    Linked to the group assignment page
    Documented what you learned from interfacing an input device(s) to microcontroller and how the physical property relates to the measured results
    Documented your design and fabrication process or linked to previous examples.
    Explained the programming process/es you used
    Explained problems and how you fixed them
    Included original design files and source code
    Included a ‘hero shot/video’ of your board

FAQ
What does "probe" mean?

    Answer: A test probe is a physical device used to connect electronic test equipment to a device under test (DUT). To probe means to measure, observe or monitor signals with test probes.

Is the satsha kit/fabduino I fabricated considered a valid board for this assignment?

    Answer: Fabricating an unmodified board is considered as electronics production. It doesn't count towards any design skill. You must make some significant changes to the original satsha kit/fabduino, for the board to be considered your own design.

Can I use a breadboard and/or jump wires to interface my input device(s)

    Answer: No. You can't use a breadboard. If you use wires, they must be soldered or connected with connectors.



assignment

   - individual assignment:
      - measure something: add a sensor to a microcontroller board that you have designed and read it
   - group assignment:
      - probe an input device's analog levels and digital signals