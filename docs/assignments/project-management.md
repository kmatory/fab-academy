#Project Management

[Assignment page](http://academy.cba.mit.edu/classes/principles_practices/index.html){target=_blank}

- (Build a personal site describing you and your final project.)[#build-a-personal-site-describing-you-and-your-final-project]
- Upload it to the class archive.
- [Work through a git tutorial](#work-through-a-git-tutorial).

Learning outcomes

    Explore and use website development tool(s)
    Identify and utilise version control protocol(s)

Have you answered these questions?

    Made a website and described how you did it
    Created a section about yourself on that website
    Added a page with a sketch and description of your final project idea(s)
    ??? Documented steps for creating your Git repository and adding files to it
    Pushed to the class GitLab
    Signed and uploaded Student Agreement


## Build a personal site describing you and your final project

Explore and use website development tool(s)

- some type of adjustable work table




## Work through a git tutorial

I went throught git tutorial here: [https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2){target=_blank}.

Here's how I set up git on my computer so I can manage my website locally and push it to the repository.

1. Install [Git for windows](https://git-scm.com/download/win){target=_blank}
2. Create a local directory to store my local copy of the website
3. Initialize in git with "git init"
4. Clone the repository I'll be working in with "git clone [http://url-to-repository]"

The main git tools I'll be using are:

1. Git pull (to get the latest changes) and merge (but there won't be any branches for mine)
2. Git status (to get status of files)
3. Git add (to add files to track)
4. Git commit (to commit the files to be pushed)
5. Git push (to push the files back to the repository)


 


Identify and utilise version control protocol(s)