#Output Devices

[A link to the assignment page](http://academy.cba.mit.edu/classes/output_devices/index.html){target=_blank}



    Group assignment:
        Measure the power consumption of an output device
        Document your work to the group work page and reflect on your individual page what you learned

    Individual assignment:
        Add an output device to a microcontroller board you've designed and program it to do something

Learning outcomes

    Demonstrate workflows used in controlling an output device(s) with MCU board you have designed

Have you answered these questions?

    Linked to the group assignment page
    Documented how you determined power consumption of an output device with your group
    Documented what you learned from interfacing output device(s) to microcontroller and controlling the device(s)
    Described your design and fabrication process or linked to previous examples.
    Explained the programming process/es you used.
    Outlined problems and how you fixed them
    Included original design files and code
    Included a ‘hero shot/video’ of your board

FAQ
What is the point of the group excercise?

    Answer: Different output devices need different amounts of energy to function. You need to be able to choose suitable power source for your projects.

Is it ok to use the LED in the hello board as an output device for this assignment?

    Answer: No, you need to do more.


assignment

   - individual assignment:
      - add an output device to a microcontroller board you've designed, and program it to do something
   - group assignment:
      - measure the power consumption of an output device