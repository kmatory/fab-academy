#Electronics Production
<h5 style="visibility: hidden;" id="top">top</h5>
<a href="#bottom" style="float: right;">Go to bottom</a>

[Assignment page](http://academy.cba.mit.edu/classes/electronics_production/index.html){target=_blank}

Group assignment:

- [Characterize the design rules for your in-house PCB production process](#characterize-the-design-rules-for-your-in-house-pcb-production-process): document [feeds](#feeds), [speeds](#speeds), [plunge rate](#plunge-rate), [depth of cut &#40;traces and outline&#41;](#depth-of-cut-traces-and-outline) and [tooling](#tooling).
    - document your work
- [Document your work to the group work page and reflect on your individual page what you learned](#document-your-work-to-the-group-work-page-and-reflect-on-your-individual-page-what-you-learned)

Individual assignment:
    
- [Make an in-circuit programmer that includes a microcontroller by milling and stuffing the PCB](#make-an-in-circuit-programmer-that-includes-a-microcontroller-by-milling-and-stuffing-the-pcb)
    - [test it to verify that it works](#test-it-to-verify-that-it-works).

Learning outcomes:

- Described the process of milling, stuffing, de-bugging and programming
- Demonstrate correct workflows and identify areas for improvement if required
    - [Milling](#milling)
    - [Stuffing](#stuffing)
    - [De-bugging](#de-bugging)
    - [Programming](#programming-the-target-board)

<!--
Have you answered these questions?

    Linked to the group assignment page
    Documented how you made (mill, stuff, solder) the board
    Documented that your board is functional
    Explained any problems and how you fixed them
    Included a ‘hero shot’ of your board


Does the in-circuit programmer need to work to complete the assignment?

    Answer: Yes. Your board must be recognized by the host computer and it must be able to communicate with the target board.

Do I have to include NC-files?

    Answer: No.
-->
<hr/> 
Machine: 

- monoFab SRM-20 

[technical specifications](https://www.rolanddga.com/products/3d/srm-20-small-milling-machine#specs){target=_blank}

    - Cuttable Material: Modeling Wax, Chemical Wood, Foam, Acrylic, Poly acetate, ABS, PC board
    - Operating Speed:  0.24 – 70.87 inch/min (6 – 1800 mm/min)
    - Spindle Rotation Speed:  Adjustable 3,000 – 7,000 rpm
    - Mechanical Resolution:  0.0000393 inches/step (0.000998594 mm/step)
    - Control Command Sets: RML-1, NC code
<hr />

## Characterize the design rules for your in-house PCB production process
I started with the [trace width line test](https://academy.cba.mit.edu/classes/electronics_production/linetest.png){target=_blank}. Toolpaths were generated using [MODS CE](http://modsproject.org/){target=_blank}.

I used the default settings in MODS excepts for the following minor changes. 

Under "Roland SRM-20 Absolute coordinates":
    
    origin: x,y,z  = 0,0,0 (mm)
    home: x,y,z  = 0,0,5 (mm)

Setting the Z to 5 mm would keep the tool at 5 mm above the material, avoiding damage if the Z was moved to the home position.

The settings below, with the exception of speeds, are part of the MODS defaults and produced consistent results for PCB milling on our monoFab SRM-20. The results can be seen [here](#trace-width-line-test). 

#### feeds
    
    - 4 mm/s

#### speeds

    - 7000 rpm

#### plunge rate

    - This was hard coded in MODS CE at 10 mm/s

#### depth of cut (traces and outline):

     - traces: 0.1016 mm / 0.004 in
     - outline: 0.6096 mm / 0.024 in

#### tooling

    - 1/64 mill: 0.39624 mm / 0.0156 in
    - 1/32 mill: 0.79248 mm / 0.0312 in

#### trace width line test

1/64" end mill

- The thinnest gap a 1/64" end mill can mill into is 0.016".
- The thinnest trace a 1/64" end mill can mill satisfactorily is 0.004".
![Trace Width Characterization](../../images/electronics_production/1-64inch-trace-width.jpg)

0.010" end mill

- The thinnest gap a 0.010" end mill can mill into is 0.011".
- The thinnest trace I'd feel comfortable with using a 0.010" end mill is the 0.003".
![Trace Width Characterization](../../images/electronics_production/0.010inch-trace-width.jpg)

## Document your work to the group work page and reflect on your individual page what you learned

Feeds, speeds, plunge rate, depth of cut and tooling will vary depending on the characterization of the machine, the tools, the materials used, and the desired outcome. Understanding of what the machine can do, it's limitations and the quality of work it can perform informs the user of how to best design for the desired outcome. This information and it's appropriate application can also extend the life of the tool and the machine. It's also useful in recognizing and troubleshooting problems that may arise. For any new materials or tools used, this characterization needs to be performed again so nominal values and settings are established for good, consistent results.

## Make an in-circuit programmer that includes a microcontroller by milling and stuffing the PCB

<!-- describe what should be done - can be done with demonstrating -->
<ul>
    <li>Described the process of milling, stuffing, de-bugging and programming</li>
    <li>Demonstrate correct workflows and identify areas for improvement if required</li>
</ul>

<br />
#### Milling

<u>Initial Setbacks</u>:

I made several attempts at milling the traces but I got inconsistent results. I realized the spoil bed was not level and that problem was further exacerbated by the concave properties of the PCB I had on hand I made a new spoil bed, leveled it and started over.

<u>Reference</u>:

I used [Brian](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html){target=_blank}'s FabISP as a reference and acquired the traces [there](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html){target=_blank}.


<div style="text-decoration-line: underline; text-decoration-style: single; margin-bottom: 5px;">Downloads:</div>
<div style="background-color: #f2f6fc;margin-bottom: 10px;">
    <div style="float:left;height:150;padding-right:30px;">Trace:</div>
    <a href="../../images/electronics_production/fts_mini_traces.png" target="_blank">
        <img alt="FABISP Trace" src="../../images/electronics_production/fts_mini_traces.png" width="150">
    </a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="float:left;height:150;padding-right:20px;">Outline:</div>
    <a href="../../images/electronics_production/fts_mini_cut.png" target="_blank">
        <img alt="FABISP Trace" src="../../images/electronics_production/fts_mini_cut.png" width="150">
    </a>
</div>

<br />

Generating toolpaths

To mill the board, I needed to generate toolpaths from the traces. I used [MODS CE](http://modsproject.org/){target=_blank} to generate this code. 

I used the default settings in MODS excepts for the following minor changes. 

Under "Roland SRM-20 Absolute coordinates":
    
    origin: x,y,z  = 0,0,0 (mm)
    home: x,y,z  = 0,0,5 (mm)

Setting the Z to 5 mm would keep the tool at 5 mm above the material, avoiding damage if the Z was moved to the home position.

Here are the steps I followed to generate toolpaths. 

In MODS, load the "Roland Monofab PCB program":

    Right click > programs > open program > Under "Roland : SRM-20 mill" > PCB CBA

To generate toolpaths, I did the following:

    1. Under "read png", click "select png file" and pointed it to the interior traces file 
        * be sure to do the outline file also

    2. set PCB defaults > mill traces (1/64)
        * Choose "mill outline (1/32)" for the outline file

    3. mill raster 2D > offset number = 0 (I wanted a clean board)
        * use only for the interior traces

    4. Roland SRM-20 Absolute coordinates > set origin: x,y,z to 0,0,0
    5. Roland SRM-20 Absolute coordinates > set home: x,y,z to 0,0,5
        * again, I set the Z home to 5mm as a cautionary measure

    6. on/off > outputs out (to save the toolpath as an RML file)
    7. mill raster 2D > calculate (generate toolpaths)


![modsce-screenshot.jpg](../../images/electronics_production/modsce-screenshot.jpg)

Start with milling the interior traces first.

1) Mount the PCB to the spoil bed.

2) Install the 1/64 in mill in the collet.

![monofab-setting-xyz.jpg](../../images/electronics_production/monofab-setting-xyz.jpg)

3) Start VPanel, make sure the tool is at a safe travel height, move gantry to an X,Y location relative to where the interior traces will be milled.

4) In VPanel, under "Set Origin Point", click "X/Y" button to register the X/Y origin point. For the Z, determine the best spot to register and move the gantry there. Once there, loosen the nut and slowly release the bit. Slowly lower it until it rests on the surface of the PCB and re-tighten the bit. Now register the Z level by cliking the "Z" button under "Set Origin Point". After registering the origin, raise the Z level to a travel safe height by clicking the "Z+" button before moving on. This will help prevent any accidental damage to the tool should someone start moving the gantry.

![monofab-setting-xyz.jpg](../../images/electronics_production/vpanel-set-origin-point.jpg)

5) Now it's time to load the RML files which contain the toolpath g-code. 

        1. Click [Cut]
        2. Click [Delete All] (if there is anything left in the "Output File List")
        3. Click [Add]
        4. Locate and click the interior traces RML file
        5. Click [Output] (this will start the milling)

![vpanel-toolpaths-loading.jpg](../../images/electronics_production/vpanel-toolpaths-loading.jpg)

Initially I had terrible results due to an unlevel spoil bed. After replacing the bed, I got this result on the first attempt.

![brian-fabisp-milled-board.jpg](../../images/electronics_production/brian-fabisp-milled-board.jpg)

6) Milling the outline is similar to milling the interior. Once the interior is done, I like to vacuum out the debris. Then do the following:

        1. Change the mill to 1/32.
        2. Make sure the mill is at a safe travel height. Move the gantry to where the Z origin will be registered.
            * Do not change the X,Y origin registration     
        3. Safely lower the end mill to the surface of the PCB and Register the Z.
        4. Click [Cut] > [Delete All] > [Add] > load the outline RML file
        5. Click [Output] to start the milling

After extracting the board from the bed, I wiped it with a cleaning wipe as a method to deburr and clean it of any oils and fingerprints.

![brian-fabisp-extracted-board.jpg](../../images/electronics_production/brian-fabisp-extracted-board.jpg)

### Stuffing

I started with getting the components together to make sure I had what I needed to complete the programmer.

Components List:

  - 1 x ATtiny45
  - 2 x 1 k&ohm; resistors
  - 2 x 499&ohm; resistors
  - 2 x 49&ohm; resistors
  - 2 x 3.3v Zener diodes
  - 1 x red LED
  - 1 x green LED
  - 1 x .1&micro;F capacitor
  - 1 x (2x3) pins

When soldering, I lightly tacked the component on one end with a little solder to hold it in place. Once sturdy, I went around to each pin/pad and finished the joints. After stuffing the board, I performed a continuity check across the components. I also placed a thin piece of cardstock to the back of the board to thicken it so the USB leads will have better connectivity in the USB port.

![brian-fabisp-stuffed-board.jpg](../../images/electronics_production/brian-fabisp-stuffed-board.jpg)



### Flashing

- make
- make flash
- make fuses

output:

    $ make
    avr-gcc -mmcu=attiny45 -Wall -DF_CPU=16500000UL -I. -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums -Os -Iusbdrv -c main.c -o main.o
    main.c:109:13: warning: always_inline function might not be inlinable [-Wattributes]
     static void delay ( void )
                 ^
    avr-gcc -mmcu=attiny45 -Wall -DF_CPU=16500000UL -I. -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums -Os -Iusbdrv -c usbdrv/usbdrv.c -o usbdrv/usbdrv.o
    avr-gcc -mmcu=attiny45 -Wall -DF_CPU=16500000UL -I. -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums -Os -Iusbdrv -c usbdrv/oddebug.c -o usbdrv/oddebug.o
    avr-gcc -x assembler-with-cpp -mmcu=attiny45 -Wall -DF_CPU=16500000UL -I. -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums -Os -Iusbdrv -c usbdrv/usbdrvasm.S -o usbdrv/usbdrvasm.o
    avr-gcc -mmcu=attiny45 -o fts_firmware.elf main.o usbdrv/usbdrv.o usbdrv/oddebug.o usbdrv/usbdrvasm.o
    avr-size -C --mcu=attiny45 fts_firmware.elf
    AVR Memory Usage
    ----------------
    Device: attiny45

    Program:    2470 bytes (60.3% Full)
    (.text + .data + .bootloader)

    Data:         75 bytes (29.3% Full)
    (.data + .bss + .noinit)


    avr-objcopy -j .text -j .data -O ihex fts_firmware.elf fts_firmware.hex

    $ make flash
    avrdude -p attiny45 -c atmelice_isp -P usb -e \
                    -U flash:w:fts_firmware.hex

    avrdude: AVR device initialized and ready to accept instructions

    Reading | ################################################## | 100% 0.00s

    avrdude: Device signature = 0x1e9206 (probably t45)
    avrdude: erasing chip
    avrdude: reading input file "fts_firmware.hex"
    avrdude: input file fts_firmware.hex auto detected as Intel Hex
    avrdude: writing flash (2470 bytes):

    Writing | ################################################## | 100% 0.81s

    avrdude: 2470 bytes of flash written
    avrdude: verifying flash memory against fts_firmware.hex:
    avrdude: load data flash data from input file fts_firmware.hex:
    avrdude: input file fts_firmware.hex auto detected as Intel Hex
    avrdude: input file fts_firmware.hex contains 2470 bytes
    avrdude: reading on-chip flash data:

    Reading | ################################################## | 100% 0.74s

    avrdude: verifying ...
    avrdude: 2470 bytes of flash verified

    avrdude done.  Thank you.

    $ make fuses
    avrdude -p attiny45 -c atmelice_isp -P usb \
                    -U lfuse:w:0xE1:m -U hfuse:w:0xDD:m \
                    -U efuse:w:0xFF:m

    avrdude: AVR device initialized and ready to accept instructions

    Reading | ################################################## | 100% 0.00s

    avrdude: Device signature = 0x1e9206 (probably t45)
    avrdude: reading input file "0xE1"
    avrdude: writing lfuse (1 bytes):

    Writing | ################################################## | 100% 0.09s

    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xE1:
    avrdude: load data lfuse data from input file 0xE1:
    avrdude: input file 0xE1 contains 1 bytes
    avrdude: reading on-chip lfuse data:

    Reading | ################################################## | 100% 0.00s

    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    avrdude: reading input file "0xDD"
    avrdude: writing hfuse (1 bytes):

    Writing | ################################################## | 100% 0.09s

    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDD:
    avrdude: load data hfuse data from input file 0xDD:
    avrdude: input file 0xDD contains 1 bytes
    avrdude: reading on-chip hfuse data:

    Reading | ################################################## | 100% 0.00s

    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing efuse (1 bytes):

    Writing | ################################################## | 100% 0.09s

    avrdude: 1 bytes of efuse written
    avrdude: verifying efuse memory against 0xFF:
    avrdude: load data efuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip efuse data:

    Reading | ################################################## | 100% 0.00s

    avrdude: verifying ...
    avrdude: 1 bytes of efuse verified

    avrdude done.  Thank you.


I disconnected my USBtiny and my Atmelice programmer from my laptop and then plugged the USBtiny back in. Now my programmer shows up in the device manager, however, I want to point out the exclamation mark beside it. Generally this means Windows has a problem with the device. In this case, I suspected Windows needed a driver to communicate with it.

![USBTinySPI shows up in device manager](../../images/electronics_production/device-manager-usbtinyspi.jpg)

I was satisfied that the programmer was functioning properly despite the Windows error, so I blew the reset fuse and removed the solder bridge so it would function as a programmer.

#### Blowing the reset fuse:

- make rstdisbl

output:

    $ make rstdisbl
    avrdude -p attiny45 -c atmelice_isp -P usb \
                    -U lfuse:w:0xE1:m -U hfuse:w:0x5D:m \
                    -U efuse:w:0xFF:m

    avrdude: AVR device initialized and ready to accept instructions

    Reading | ################################################## | 100% 0.00s

    avrdude: Device signature = 0x1e9206 (probably t45)
    avrdude: reading input file "0xE1"
    avrdude: writing lfuse (1 bytes):

    Writing | ################################################## | 100% 0.09s

    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xE1:
    avrdude: load data lfuse data from input file 0xE1:
    avrdude: input file 0xE1 contains 1 bytes
    avrdude: reading on-chip lfuse data:

    Reading | ################################################## | 100% 0.00s

    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    avrdude: reading input file "0x5D"
    avrdude: writing hfuse (1 bytes):

    Writing | avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_recv(): Inconsistent fragment number; expect 1, got 0
    avrdude: stk500v2_jtag3_recv(): error in jtagmkII_recv()
    ################################################## | 100% 0.31s

    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0x5D:
    avrdude: load data hfuse data from input file 0x5D:
    avrdude: input file 0x5D contains 1 bytes
    avrdude: reading on-chip hfuse data:

    Reading | avrdude: jtag3_edbg_send(): Unexpected response 0x81, 0x11
    avrdude: jtag3_edbg_recv(): Unexpected response 0x80
    avrdude: stk500v2_jtag3_recv(): error in jtagmkII_recv()
    ################################################## | 100% 0.24s

    avrdude: verifying ...
    avrdude: verification error, first mismatch at byte 0x0000
             0x58 != 0x5d
    avrdude: verification error; content mismatch
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_send(): Unexpected response 0xe0, 0xf7
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_recv(): Unexpected length value (17 > 4)
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_recv(): Unexpected response 0x17
    avrdude: stk500v2_jtag3_recv(): error in jtagmkII_recv()
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_send(): Unexpected response 0x00, 0x00
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_recv(): Unexpected response 0x11
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_send(): Unexpected response 0x00, 0x00
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_recv(): Unexpected length value (17 > 4)
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_recv(): Inconsistent fragment number; expect 1, got 0
    avrdude: Short read, read only 0 out of 512 bytes
    avrdude: jtag3_edbg_signoff(): failed to read from serial port (0)

    avrdude done.  Thank you.

    make: *** [rstdisbl] Error 1


#### Checking programmer operation

As I suspected, Windows couldn't communicate with the programmer. It needs a driver.

    $ avrdude -c usbtiny -p t45
    avrdude.exe: Error: Could not find USBtiny device (0x1781/0xc9f)

    avrdude.exe done.  Thank you.

I found a driver on <a href="https://learn.adafruit.com/usbtinyisp/drivers" target="_blank">learn.adafruit.com</a>. It can be downloaded <a href="../../files/electronics_production/USBtinyISP_win_driver.zip" target="_blank">locally here</a> or <a href="https://learn.adafruit.com/usbtinyisp/drivers" target="_blank">here on learn.adafruit.com</a>. Now Windows 11 is satisfied.

![Image of FabISP programming t45-echo board](../../images/electronics_production/dm-usbtiny-driver-installed.jpg)

Below is the output from the communication test with the programmer. The response signifies the windows can now communicate with the programmer. The error "initialization failed, rc=-1" was given only because the target board wasn't actually connected to the programmer when I ran the test and it wasn't necessary at this point.

    $ avrdude -c usbtiny -p t45

    avrdude.exe: initialization failed, rc=-1
                 Double check connections and try again, or use -F to override
                 this check.


    avrdude.exe done.  Thank you.


### De-bugging

I didn't encounter any problems that needed debugging.

### Programming (the target board)

I know the <a href="http://academy.cba.mit.edu/classes/embedded_programming/index.html">T45-ECHO</a> board I made earlier works, so I used it as a test subject. The programmer is on the left in the usb extender and the t45-echo board is on the right. I flashed the t45-echo code to the board, however with some minor changes to the code.

![Image of FabISP programming t45-echo board](../../images/electronics_production/fabisp-echoboard-programming.jpg)

** Flash output **

    $ make -f hello.ftdi.45.echo.make program-usbtiny avr-objcopy -o ihex hello.ftdi.45.echo.out hello.ftdi.45.echo.c.hex
    avr-objcopy -O ihex hello.ftdi.45.echo.out hello.ftdi.45.echo.c.hex;\
            avr-size --mcu=attiny45 --format=avr hello.ftdi.45.echo.out
    AVR Memory Usage
    ----------------
    Device: attiny45

    Program:     830 bytes (20.3% Full)
    (.text + .data + .bootloader)

    Data:         64 bytes (25.0% Full)
    (.data + .bss + .noinit)


    avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.ftdi.45.echo.c.hex

    avrdude: AVR device initialized and ready to accept instructions

    Reading | ################################################## | 100% 0.01s

    avrdude: Device signature = 0x1e9206 (probably t45)
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.ftdi.45.echo.c.hex"
    avrdude: input file hello.ftdi.45.echo.c.hex auto detected as Intel Hex
    avrdude: writing flash (830 bytes):

    Writing | ################################################## | 100% 2.23s

    avrdude: 830 bytes of flash written
    avrdude: verifying flash memory against hello.ftdi.45.echo.c.hex:
    avrdude: load data flash data from input file hello.ftdi.45.echo.c.hex:
    avrdude: input file hello.ftdi.45.echo.c.hex auto detected as Intel Hex
    avrdude: input file hello.ftdi.45.echo.c.hex contains 830 bytes
    avrdude: reading on-chip flash data:

    Reading | ################################################## | 100% 3.57s

    avrdude: verifying ...
    avrdude: 830 bytes of flash verified

    avrdude done.  Thank you.

    make: *** No rule to make target `avr-objcopy'.  Stop.


#### Test it to verify that it works

I added ** "USBTiny Programmer Test -" ** to the beginning of the echo to differentiate it from what was already on the chip and to denote a successful flash.

![Proof programming t45-echo board with usbtiny is successful](../../images/electronics_production/brian-fabisp-proof.jpg)

### Hero Shot
![fabisp-hero-shot.jpg](../../images/electronics_production/fabisp-hero-shot.jpg)



<div style="text-decoration-line: underline; text-decoration-style: double; margin-bottom: 5px;"><h4>Downloads:</h4></div>

<div style="background-color: #f2f6fc;margin-bottom: 10px;">
    <div style="float:left;height:150;padding-right:30px;">Trace:</div>
    <a href="../../images/electronics_production/fts_mini_traces.png" target="_blank">
        <img alt="FABISP Trace" src="../../images/electronics_production/fts_mini_traces.png" width="150">
    </a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="float:left;height:150;padding-right:20px;">Outline:</div>
    <a href="../../images/electronics_production/fts_mini_cut.png" target="_blank">
        <img alt="FABISP Trace" src="../../images/electronics_production/fts_mini_cut.png" width="150">
    </a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="float:left;height:150;padding-right:20px;">Firmware Download:</div>
    <a href="../../files/electronics_production/fts_firmware_bdm_v1.zip" target="_blank">Click here</a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="float:left;height:150;padding-right:20px;">Windows Driver:</div>
    <a href="../../files/electronics_production/USBtinyISP_win_driver.zip" target="_blank">here</a> 
    or <a href="https://learn.adafruit.com/usbtinyisp/drivers" target="_blank">here</a>
</div>
<h5 style="visibility: hidden;" id="bottom">bottom</h5>

[Go to top](#top)
