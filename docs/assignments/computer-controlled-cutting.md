# Computer Controlled Cutting

[Assignment page](http://academy.cba.mit.edu/classes/computer_cutting/index.html){target=_blank}

Group assignment:
  
  - [Characterize your lasercutter's](#characterize-your-lasercutter)
    - [focus](#focus) 
    - [power](#power) 
    - [speed](#speed) 
    - [rate](#rate-or-frequency) 
    - [kerf](#kerf)
    - [joint clearance](#joint-clearance)
  - [Document your work to the group work page and reflect on your individual page what you learned](#document-your-work-to-the-group-work-page-and-reflect-on-your-individual-page-what-you-learned)

Individual assignments

 - [Design, lasercut, and document a parametric press-fit construction kit, which can be assembled in multiple ways. Account for the lasercutter kerf](#design-lasercut-kit)

Cut something on the vinylcutter

<!--
Learning outcomes

    Demonstrate and describe parametric 2D modelling processes
    Identify and explain processes involved in using the laser cutter.
    Develop, evaluate and construct the parametric construction kit
    Identify and explain processes involved in using the vinyl cutter.

Have you answered these questions?

    Linked to the group assignment page
    Explained how you parametrically designed your files
    Documented how you made your press-fit kit
    Documented how you made your vinyl cutting
    Included your original design files
    Included your hero shots


Demonstrate and describe parametric 2D modelling processes

 - Explained how you parametrically designed your files


Create a set that uses 3D created objects with fitting joints
-->

## Characterize your lasercutter

This lab has an Epilog Fusion M2 75 watt CO2 laser machine.  The bed size is 32"L x 20"H.


### Focus

Focus refers to the waist of the laser and its relationship to the material used. The waist is the smallest point of laser where the power is most optimal. The placement of the laser's waist in relationship to the material determines what effect the laser has on the material. 

In regards to our machine, this is a setting that can adjust the distance of the table from the focus lens and thereby adjusting the focus of the laser. This setting uses increments of 10 and each increment of ten will change the table height by 0.001” (0.0254 mm).


### Power

The total power of our machine is 75 watts. Power can be set in percentages of that 75 watts and is adjustable in 1% increments from 1-100%. For example, if I sent a job that uses 80% power, then the machine would use 60 watts of power for that job (0.80 x 75 = 60).

### Speed

The speed refers to the travel speed of the carraige and is adjustable in 1% increments from 1-100%. The slower the speed, the more any given point on the path is exposed to the power and/or frequency of the laser, resulting in the deeper cuts or engraving. The speed is heavily dependent on the hardness and the thickness of the material being used. Slower speed settings will also produce better edge quality when cutting.

### Rate (or frequency)

The number of laser pulses that the laser fires per inch of travel and can be adjusted from 1 to 100. A lower frequency number will have the effect of less heat because fewer pulses are being used to cut the material. Lower frequency rates are helpful for products like wood, where charring is evident at higher frequencies. High frequencies are useful on materials like acrylic where a large amount of heat is desirable to melt or flame polish the edges.

### Kerf

The following is the approach I used to determine the laser's kerf:

* Using 0.19" cardboard, cut a series of 6 squares
* Line up the squares and measure the combined length
* Measure the negative space
* Do the math to determine the kerf

Using the rectangle tool in CorelDraw, I drew 6 squares with a line thickness of 0.004" (0.101 mm) or less, which is considered hairline thickness. Hairline thickness tells the lasercutter these are vector toolpaths, not raster and therfore to cut, not engrave.

![Lasercutter - CorelDraw - Drawing squares for kerf meauserment](../../images/computer_controlled_cutting/coreldraw_squares1.jpg){width=500}

<br />
I used following settings on the lasermachine for this job:

  - Speed:  23%
  - Power: 90%
  - Frequency: 60%
  - Focus: on the material surface

One thing to note is that in this process to determine the laser kerf, the laser was focused on the surface of the cardboard. I believe this is important because the waist of the beam, which is the thinnest part of the laser, is on the surface of the material, so the kerf is 

![Lasercutter - CorelDraw - Squares Cut](../../images/computer_controlled_cutting/kerf_squares1.jpg)

<br />I extracted the squares, lined them up side by side to measure their combined length.<br />

![Lasercutter - CorelDraw - Positive Material Length Measurement](../../images/computer_controlled_cutting/kerf_measure1.jpg)


<br />Next, I measured the length of the negative space left behind. <br />

![Lasercutter - CorelDraw - Negative Space Length Measurement](../../images/computer_controlled_cutting/kerf_negative_measurement.jpg)


The lateral negative space the squares left behind will be longer than the combine length of the squares. Subtracting the combined length from the negative space will give me the total kerf width.

    Combined length measurement: 151.3 mm
    Negative space measurement:  152.5 mm

    Subtract the combined length from the length of the negative space:
      152.5 mm - 151.3 mm = 1.2 mm

I have the total kerf width, however, there are 7 edges. I need to know the kerf for one edge. To get that number, I divide the total kerf by the number of edges, which again, is 7.

    Divide the difference by the total number of edges, which is 7 in this case:    
      1.2 mm / 7 (total number of edges: 6 squares - 7 edges) = 0.1714 mm

The kerf in milimeters is 0.1714. I measured in milimeters for better accuracy since I'm dealing with such small measurements. To convert to inches, I divide 0.1714 by 25.4.

    Divide by 25.4 to convert milimeters to inches:
      0.1714 / 25.4 (divided by 25.4 to get inches) = 0.0067 in

      My kerf size is 0.1714 mm (0.0067 in)


I went througth this process a second time for accuracy's sake and the resulting kerf was 0.1857 mm (0.0073 in). <br />

    If I average the 2: 

    (0.1714 mm + 0.1857 mm)/ 2 = 0.17855 mm

    I get a kerf size of 0.17855 mm (0.007 in).


### Joint Clearance:

Here I ran a test to determine the amount of clearance, or compression in this case, to acheive a good compression fit joint. I made each slot slightly smaller than the thickness of the cardboard (0.19 in). 

![cardboard joint tolerance test](../../images/computer_controlled_cutting/tolerance-all.jpg)

I found that undersizing the slot by 0.02" which yields a slot width of 0.17" makes a good compression fit.

<div style="clear:clear;background-color: gray;">
    <a href="../../images/computer_controlled_cutting/tolerance-1.jpg" style="float:left;width:225px;" target="_blank">
        <img alt="tolerance-1.jpg" src="../../images/computer_controlled_cutting/tolerance-1.jpg"/>
    </a>
    <a href="../../images/computer_controlled_cutting/tolerance-2.jpg" style="float:left;width:225px;" target="_blank">
        <img alt="tolerance-2.jpg" src="../../images/computer_controlled_cutting/tolerance-2.jpg" />
    </a>
    <a href="../../images/computer_controlled_cutting/tolerance-3.jpg" style="float:left;width:225px;" target="_blank">
        <img alt="tolerance-3.jpg" src="../../images/computer_controlled_cutting/tolerance-3.jpg" />
    </a>
</div>

<br style="clear: both;"/>

### Document your work to the group work page and reflect on your individual page what you learned



<h2 id="design-lasercut-kit">Design, lasercut, and document a parametric</h2>

    Design Software: Fusion 360

Unsure where to begin with a construction kit, I started with basic shapes, square, rectangle, triangle, circle, added slots and fingers, and expanded from there. Here is an overview of the parts that were created in this kit.

![Press-Fit Construciton Kit - Designing in CAD](../../images/computer_controlled_cutting/conkit-components-overview.png)

The laser cuts directly on the toolpath thus altering the dimensions of the project, so has to be accounted for in the design phase. Take for example the piece in the image below where the "Width" is 1.007". It's dimensioned to be 1" wide. The laser kerf will decrease this by 0.007". To compensate, I added a full kerf to the width on the front end which results in a width of 1.007". When this is cut on the laser machine, the width will be 1". This same approach was applied to the height.

As for the slots, they are based on the thickness of the cardboard, which is 0.19". The laser kerf increases this width. This will cause the slots to be far too wide. To compensate, I have to subtract the kerf on the front end. Additionally, to get the right compression, I also need to subtract the clearance, which is another 0.02", from that already kerf adjusted 0.19 inch. The resulting width of the slot is 0.165". When this is cut on the laser machine, the width will be 0.17", which is the thickness minus the clearance (0.19" - 0.02").

![conkit-example-component-1.jpg](../../images/computer_controlled_cutting/conkit-components-overview-1.jpg)

To export this construction kit, right click on name of the project > click "create drawing" > Ok.

![f360-create-drawing.jpg](../../images/computer_controlled_cutting/f360-create-drawing.jpg)

I chose the "TOP" orientation and "1:1" scale. Under export click "export as dxf".

![f360-export-dxf.jpg](../../images/computer_controlled_cutting/f360-export-dxf.jpg)

Corel draw - Imported the DXF file into Corel Draw and set the line thickness to "hairline" for cutting.

![conkit-coreldraw-overiew-1.png](../../images/computer_controlled_cutting/conkit-coreldraw-overiew-1.png)

Sending the print job to the laser machine

![epilog-laser-print-dialog.png](../../images/computer_controlled_cutting/epilog-laser-print-dialog.png)

The finished job

![conkit-results-1.jpg](../../images/computer_controlled_cutting/conkit-results-1.jpg)

<hr style="height:2px;border-width:0;color:gray;background-color:gray">



<!-- END -- Design, lasercut, and document a parametric press-fit construction kit, which can be assembled in multiple ways -->

## Cut Something on the Vinylcutter

I started with installing the appropriate drivers and software.
Once up and running, I made some test cuts with the CAMM-1 GS-24 to determine the appropriate cut depth and speed.

Below is how I set up and tested the machine before I cut my image.

1) Load and guide the material into the feeder

![Vinyl Cutter - Load Material](../../images/computer_controlled_cutting/loading_vc_material.jpg)


2) Set the pinch roller on the material according to where you want to cut and engage the loading lever to clamp the pinch rollers down

3) Power on the device
  
4) The machine will display "SELECT SHEET: a Roll, Edge or Piece" (Roll in my case). Select with up,down arrows and press "Enter"

![Vinyl Cutter - Select Sheet](../../images/computer_controlled_cutting/vc_select_sheet.jpg)



  5. The machine runs the carraige across the length of the feeder and scans the sheet and the location of the rollers
  6. The machine prints on the LCD screen the distance of the rollers from each other, the cut force that will be used, and the offset
  7. Using the arrows, move the vinyl forwards or back until it is where you want the edge to be in relation to the cutter head
  8. Once the vinyl is set, press and hold the "Origin" button to set the origin
  9. Press and hold "Test" to perform a test cut

<br />

> I performed several test cuts until I was able to shear away the surrounding vinyl without pulling up the surrounding pieces.

![Vinyl Cutter - Final Test Cut](../../images/computer_controlled_cutting/final-test-cut.jpg)


Note: The force, speed or offset can be changed by using the arrow keys to navigate.

  - MENU 
  - Scroll down to CONDITION
  - Right Arrow to select CONIDITON
  - Scroll up or down to either SPEED, FORCE or OFFSET
  - Right Arrow to select
  - Use up, down arrow to modify
  - Press "ENTER" to save changes


The Vinyl Cutter was already set with a Pen Force of +2. When I ran my test cut, I found that these settings worked well with the Pen Force at +2.

<br />

# Setting Up the Design in CutStudio

I took an image of the movie Grimlock statue from the <a href="https://www.sideshow.com/collectibles/transformers-grimlock-prime-1-studio-910874" target="_blank">Sideshow.com website</a> and using Krita, I simplified it by making a silhouette. I also added some lettering on the base and saved it in a vector format. Download the original image <a href="../../images/computer_controlled_cutting/sideshow-movie-grimlock-statue.jpg" target="_blank">here</a>.

![CutStudio - Setting up design](../../images/computer_controlled_cutting/vinyl-design.jpg){width=325 align=left}
![CutStudio - Setting up design](../../images/computer_controlled_cutting/vinyl-design-prep.jpg){width=325}

<br style="clear:both"/>

To create the outline to cut:
> Import the design into CutStudio

![CutStudio - CutStudio Prep](../../images/computer_controlled_cutting/cutstudio-prep.jpg){width=500}

> Right click -> "Image Outline" 

![CutStudio Image Outline](../../images/computer_controlled_cutting/rc-image-outline.jpg)

> Click "Extract Contour Lines"

![CutStudio Contour LInes](../../images/computer_controlled_cutting/cutstudio-contour-lines.png){width=400}

> I moved the outline over to the side, selected and deleted the original image.<br />
> Click "Move to origin" <img src="../../images/computer_controlled_cutting/move-to-origin.jpg" /> to align the outline to the bottom, left corner of the screen, which represents the front, left corner of the material coming through the machine feeder.

![CutStudio - CutStudio Ready /cut](../../images/computer_controlled_cutting/cutstudio-ready-cut.jpg)

> From the menu bar, click Cutting

![CutStudio - Send Cut to Device](../../images/computer_controlled_cutting/cutstudio-send-cut.jpg)

> Lastly, import the width dimension set earlier using the pinch rollers on the machine. <br />
> Click "Change" &#62; "Change" &#62; "Get from Machine" <br />
> Then Click "OK" on all screens to send to the CAMM-1 GS-24

![CutStudio - Import Dimensions](../../images/computer_controlled_cutting/cutstudio-send-setup.jpg)

> I laid the entire piece out on an acrylic sheet and then
weeded by sheering the unwanted material. <br />

![grimlock-weeding-1.jpg](../../images/computer_controlled_cutting/grimlock-weeding-1.jpg)

![grimlock-weeding-2.jpg](../../images/computer_controlled_cutting/grimlock-weeding-2.jpg)

> I started with a smaller version to practice with before making the larger version.

![finished-piece.jpg](../../images/computer_controlled_cutting/finished-piece.jpg)

![grimlock-done.jpg](../../images/computer_controlled_cutting/grimlock-done.jpg)

<!-- -->

## &#35;Downloads

<div style="background-color: #f2f6fc;margin-bottom: 10px;">
    <div style="float:left;height:150;padding-right:30px;">Construction Kit Fusion 360 CAD File:</div>
    <a href="../../files/computer_controlled_cutting/conkit.f3d" target="_blank">
        <img alt="download" src="../../images/computer_controlled_cutting/conkit-components-overview-1.jpg" width="150" />
    </a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 10px;">
    <div style="float:left;height:150;padding-right:30px;">CutStudio Design File:</div>
    <a href="../../files/computer_controlled_cutting/Grimlock3_10.5in.cst" target="_blank">
        <img alt="download" src="../../images/computer_controlled_cutting/vinyl-design-prep.jpg" width="150" />
    </a>
</div>
