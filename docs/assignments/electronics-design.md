#Electronic Design

[A link to the assignment page](http://academy.cba.mit.edu/classes/electronics_design/index.html){target=_blank}

Milling machine: MonoFab SRM-20

<!--
- Working range of the X axis:  203.2 mm
- Working range of the Y axis:  152.4 mm
- Distance from Work plate surface to Spindle head bottom: 71 mm or 2.7952755 in
- Spindle unit mounted in Low Position: 
    - Distance from Work plate surface to Spindle: 100.75 mm
    - Working range of the spindle unit: 60.5 mm
- Spindle unit mounted in High Position:
    - Distance from Work plate surface to Spindle - 130.75 mm
    - Working range of the spindle unit: 60.5 mm
-->
Group assignment:

- Use the test equipment in your lab to observe the operation of a microcontroller circuit board (in minimum, check operating voltage on the board with multimeter or voltmeter and use oscilloscope to check noise of operating voltage and interpret a data signal)
- Document your work to the group work page and reflect on your individual page what you learned

Individual assignment:

- Redraw one of the echo hello-world boards or something equivalent, add (at least) a button and LED (with current-limiting resistor) or equivalent input and output, check the design rules, make it, test it. Optionally, simulate its operation.

Learning outcomes

- Select and use software for circuit board design
- Demonstrate workflows used in circuit board design

Have you answered these questions?

- Linked to the group assignment page
- Document what you have learned in electronics design
- Explained problems and how you fixed them, if you make a board and it doesn't work; fix the board (with jumper wires etc) until it does work.
- Include original design files (Eagle, KiCad, etc.)
- Include a ‘hero shot’ of your board
- Load a program and tested if your board works


##Assignment

* group project:
  -  use the test equipment in your lab to observe the operation
     of a microcontroller circuit board

* individual project:
  - redraw an echo hello-world board,
  - add (at least) a button and LED (with current-limiting resistor)
  - check the design rules, make it, and test that it can communicate
  - extra credit: simulate its operation

FAQ
Can I modify an existing design board?

    Answer: No, you have to create your board from scratch.

Do I need to create a schematic file?

    Answer: Yes, at least for this week.

Can I draw my design by hand?

    Answer: Yes, but you have to use EDA software for this week.


##&#35;Observe the operation of a microcontroller circuit board&#35;

Niel’s &quot;Hello.HC-SR04&quot;

[HC-SR04 Datasheet](https://www.digikey.com/htmldatasheets/production/1979760/0/0/1/hc-sr04.html)

The HC-SR04 Ultrasonic Shield utilizes ultrasonic soundwaves to determine distance from 2cm to 400cm with an accuracy of 3mm. Five volts on the Trig pin for at least 10 µs will trigger an 8 cycle, 40kHz sound pulse. If there is any object within range in the path of the pulse, an echo pulse will be bounced back. The HC-SR04 will give the time in microseconds it took for the soundwave to make a complete loop to and from the object on its Echo pin.

![Hello.HC-SR04 Input Device](../../images/electronics_design/helloHCSR04-ss1.jpg)


###&#35;check operating voltage

Multimeter

  - AMPROBE PM55A

notes: indicate what it was plugged into


Ths Hello.HC-SRO4 board is connected to a USB port of a laptop via ftdi and the voltage at the GND and VCC points reads 5V.

![Hello.HC-SR04 Voltage check with voltmeter](../../images/electronics_design/sonar-voltage-check.jpg)


###&#35;check noise of operating voltage

I started with 5V DC signal on 25ns per division time scale.

![Hello.HC-SR04 Scope Voltage Measurement](../../images/electronics_design/voltage-dc.jpg)

I changed the coupling to show only the AC noise.

![Hello.HC-SR04 AC coupling](../../images/electronics_design/voltage-ac-filtering.jpg)

I lowered the voltage scale to 50mV and the time scale to 1ns to increase resolution.
<br />The capture shows roughly 156mV peek to peek of AC noise.

![Hello.HC-SR04 Lowering voltage and time scale](../../images/electronics_design/voltage-noise-measurement.jpg)


###&#35;Interpret a data signal

Oscilloscope

  - G<u>w</u>Instek GDS-1152A-U

Below is a scope capture of serialized bits from the Hello.HC-SR04 Sonar board on the Rx pin at 9600 Baud.

![Hello.HC-SR04 Serial Databits](../../images/electronics_design/serial-databits.jpg)

At 9600 baud, each bit is approximately 1/9600 or 104&micro;s wide on the scope. If I set my time division to 100&micro;s, this would make it easier to make distinctions between individual bits. Each byte of data also carries a start and stop bits along with the 8 data bits. This makes each byte 10 bits long.

![Hello.HC-SR04 Zooming in on databits](../../images/electronics_design/serial-databits-zoom.jpg)

###&#35;Redraw one of the echo hello-world boards

Board: hello.ftdi.echo w/button and led

Concept:

Using a serial connection to the board, type out a message and push the button to have it blink out the message using morse code.

Components List:

  - 1 x ATtiny45
  - 1 x 1 k&ohm; resistors
  - 1 x 100 &ohm; resistors
  - 1 x 10 k&ohm; resistors
  - 1 x 1&micro;F capacitor
  - 1 x LED  
  - 1 x ftdi connector  
  - 1 x (2x3) pins
  - 1 x B3SN-3112P tactile switch

###&#35;Select and use software for circuit board design
- Software: Eagle 9.6.2

###&#35;Check the design rules

    - Design Rules:
        - Tool: 1/64 end mill
            - Minimum trace width: 0.004 in
            - Minimal clearance: 0.016 in
        - Tool: 0.10" end mill
            - Minimum trace width: 0.004 in
            - Minimal clearance: 0.011 in

- Project Parameters:
    - Tool: 1/64
    - Trace width: 0.010 in
    - Clearance: 0.016 in

####&#35;Demonstrate workflows used in circuit board design

I started with Neil's [t45-echo](http://academy.cba.mit.edu/classes/embedded_programming/index.html){target=_blank} [board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.45.png){target=_blank} as the base. As stated earlier, the concept is this:

    1. Connect to the board via serial (pyserial works well)
    2. Type out a message in 20 lowercase characters or less (punctuation and uppercase was not included)
    3. Press ENTER (This is important)
    4. Press the button on the board (Starts tapping out morse code on the LED and screen)

The first thing I considered was whether there are enough pins available to drive the LED and button. There are 2 pins still available on the t45, PB3 & PB4. As shown [here](../../images/electronics_design/ed-redraw-ehw-schematic.png){target=_blank}, I assigned LED to PB4 and back to ground. The button is connected to PB3 and ground.


<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="padding-right:20px;">Eagle Schematic File: <a href="../../files/electronics_design/hello.ftdi.echo.morse.sch" target="_blank">Click here to download</a></div>
    <a href="../../images/electronics_design/ed-redraw-ehw-schematic.png" target="_blank">
        <img alt="Eagle Schematic File Download" src="../../images/electronics_design/ed-redraw-ehw-schematic.png"/>
    </a>
</div>

####Routing traces

For me, autorouting has been a good place to start with placing my traces. I'd adjust component placement, autoroute again and manual reroute until I get the traces in a usable configuration. This is the final board layout.

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="padding-right:20px;">Component Layout: <a href="../../files/electronics_design/hello.ftdi.echo.morse.brd" target="_blank">Download Board (.brd)</a> | <a href="../../images/electronics_design/hello.ftdi.echo.morse-traces.png" target="_blank">Download Trace</a> | <a href="../../images/electronics_design/hello.ftdi.echo.morse-outline.png" target="_blank">Download Outline</a></div>
    <a href="../../images/electronics_design/hello.ftdi.echo.morse-board.png" target="_blank">
        <img alt="board image" src="../../images/electronics_design/hello.ftdi.echo.morse-traces.png" />
    </a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <b>Additional view:</b>
    <p style="padding-right:20px;">Shown below, I thought it useful to modify the traces in GIMP to look like Neil's boards that show component placement.</p>
    <a href="../../images/electronics_design/hello.ftdi.echo.morse-board.png" target="_blank">
        <img alt="board image" src="../../images/electronics_design/hello.ftdi.echo.morse-board.png" />
    </a>
</div>

Finally, I used the rectangle tool to create the outside dimension of the board on the Dimensions layer. To export, I hid all layers except Top and Dimensions, set the image to monochrome with a resolution of 1500 dpi.

Problem: Below is the first board I milled. This trace was exported at 500 dpi per the tutorial. Ignoring the burrs for a moment, the angled traces (near the center of the board) are rather rough. There is also a spot where there wasn't enough clearance for the tool to get through (circled in red). I went back and adjusted several traces thus increasing the clearance.

![500dpi_traces.jpg](../../images/electronics_design/500dpi_traces.jpg){width=600}

Here is the resulting milled board after making adjustments. I'm not sure if the dpi affects how the toolpaths are generated and smoothness of the resulting milled traces, but I change it anyway. I went back and looked at the traces in the hello world boards and they are 1000 dpi. I exported the final trace at 1500 dpi but I 1000 dpi is sufficient. The angled traces are visually much cleaner compared to the previous. This cleaner result may be a combination of increasing the clearance between the traces in the problem area and increasing the dpi.

![redraw-ehw-milled-board.jpg](../../images/electronics_design/redraw-ehw-milled-board.jpg){width=600}


Below is the final board, stuffed and ready to be programmed. Additionally, I used a multimeter to verify continuity across traces and between components.

![redraw-ehw-stuffed-board.jpg](../../images/electronics_design/redraw-ehw-stuffed-board.jpg){width=600}

##&#35;Load a program and test if your board works

As an initial test, I flashed the original [code](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.45.echo.c){target=_blank} for the t45-echo board using my Atmel-ICE programmer. The addition of the LED and button shouldn't have any affect on this code and so this code should run properly, and it did.

![Initial test of hello.ftdi.45.echo.morse board](../../images/electronics_design/helloftdi45echo-first-test-proof.jpg)


##&#35;Hero shot

![hero-shot-hello.ftdi.45.echo.morse.jpg](../../images/electronics_design/hello.ftdi.45.echo.morse-hero-shot.jpg)


##&#35;Downloads

<div style="background-color: #f2f6fc;margin-bottom: 10px;">
    <div style="float:left;height:150;padding-right:30px;">Traces:</div>
    <a href="../../images/electronics_design/hello.ftdi.echo.morse-traces.png" target="_blank">
        <img alt="download traces" src="../../images/electronics_design/hello.ftdi.echo.morse-traces.png" width="150" />
    </a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="float:left;height:150;padding-right:20px;">Outline:</div>
    <a href="../../images/electronics_design/hello.ftdi.echo.morse-outline.png" target="_blank">
        <img alt="download outline" src="../../images/electronics_design/hello.ftdi.echo.morse-outline.png" width="150" />
    </a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="float:left;height:150;padding-right:20px;">Eagle Schematic (.sch)</div>
    <a href="../../files/electronics_design/hello.ftdi.echo.morse.sch" target="_blank">
        <img alt="download outline" src="../../images/electronics_design/ed-redraw-ehw-schematic.png" width="350" />
    </a>
</div>

<div style="background-color: #f2f6fc;margin-bottom: 5px;">
    <div style="float:left;height:150;padding-right:20px;">Eagle Board (.brd)</div>
    <a href="../../files/electronics_design/hello.ftdi.echo.morse.brd" target="_blank">
        <img alt="download outline" src="../../images/electronics_design/ed-redraw-ehw-board.png" width="150" />
    </a>
</div>

[&#91;Link to Neil's t45-echo code &#93;](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.45.echo.c){target=_blank}



