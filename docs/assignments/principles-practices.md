#Principles and Practices
   
[A link to the assignment page](http://academy.cba.mit.edu/classes/project_management/index.html){target=_blank}

    Plan and sketch a potential final project

Learning outcomes

    Communicate an initial project proposal

Have you answered these questions?

    Sketched your final project idea/s
    Described briefly what it will do and who will use it

FAQ
How much is "briefly"?

    Answer: At least one sketch (something visual) and one paragraph (a few senteces) of text.

